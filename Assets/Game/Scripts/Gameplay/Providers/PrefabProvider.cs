﻿using System;
using Common.ObjectPool;
using Gameplay.Weapons.Bullets;
using ScriptableObjects.Classes.Prefabs;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;
using static Common.Enums;

namespace Gameplay.Providers
{
	public class PrefabProvider : Singleton<PrefabProvider>
	{
		[SerializeField, AssetList] private PrefabData _prefabData;

		public static PooledParticle GetParticlePrefab(ParticleType type)
		{
			if (!Instance._prefabData.ParticlePrefabs.ContainsKey(type))
			{
				throw new NullReferenceException($"Check Particle Prefab Data {type}");
			}

			return Instance._prefabData.ParticlePrefabs[type];
		}
		
		public static BulletBase GetBulletPrefab(BulletType type)
		{
			if (!Instance._prefabData.BulletPrefabs.ContainsKey(type))
			{
				throw new NullReferenceException($"Check Bullet Prefab Data {type}");
			}
            
        
			return Instance._prefabData.BulletPrefabs[type];
		}
	}
}