﻿using System;
using UnityEngine;

namespace Gameplay.LevelsLogic
{
    public class LevelController : MonoBehaviour
    {
        public event Action OnNextLocation;
        
        [SerializeField] private ExitLevel _exitLevel;


        private void OnEnable()
        {
            _exitLevel.OnPlayerExit += NextLocation;
        }

        private void OnDisable()
        {
            _exitLevel.OnPlayerExit -= NextLocation;
        }

        private void NextLocation()
        {
            OnNextLocation?.Invoke();
        }
    }
}