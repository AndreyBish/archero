﻿using System;
using Gameplay.Characters;
using Gameplay.Characters.Player;
using UnityEngine;

namespace Gameplay.LevelsLogic
{
    public class ExitLevel : MonoBehaviour
    {
        public event Action OnPlayerExit;
        
        [SerializeField] private InteractionZone _exitZone;

        private void OnEnable()
        {
            _exitZone.OnZoneEnter += OnZoneEnter;
            _exitZone.OnZoneExit += OnZoneExit;
        }

        private void OnDisable()
        {
            _exitZone.OnZoneEnter -= OnZoneEnter;
            _exitZone.OnZoneExit -= OnZoneExit;
        }
        
        private void OnZoneEnter(Collider other)
        {
            if (other.transform.GetComponent<Player>()) ActiveExit();
        }

        private void OnZoneExit(Collider other)
        {
            if (other.transform.GetComponent<Player>()) DisableExit();
        }

        private void ActiveExit()
        {
            OnPlayerExit?.Invoke();
        }

        private void DisableExit()
        {
            
        }
    }
}