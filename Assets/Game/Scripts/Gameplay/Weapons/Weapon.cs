﻿using Extensions;
using Game.ScriptableObjects.Classes.WeaponsData;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;

namespace Gameplay.Weapons
{
    public class Weapon : MonoBehaviour
    {
        [SerializeField, GroupComponent] private MeshRenderer _meshRenderer;
        [SerializeField, AssetList, OnValueChanged(nameof(UpdateWeapon)), GroupSetting] protected WeaponData _data;
        public WeaponData Data => _data;
        
        private void Awake()
        {
            UpdateWeapon();
        }

        private void OnEnable()
        {
            WeaponData.OnChangeOffset += ChangeOffset;
        }

        private void OnDisable()
        {
            WeaponData.OnChangeOffset -= ChangeOffset;
        }

        public virtual void Fire(Vector3 targetPosition)
        {
        }

        private void UpdateWeapon()
        {
            //_meshFilter.sharedMesh = _data.Mesh;
            ChangeOffset(_data);
        }

        protected virtual void ChangeOffset(WeaponData weaponData)
        {
            if (weaponData != _data) return;

            transform.localPosition = _data.PositionOffset;
            transform.localEulerAngles = _data.RotationOffset;
        }

        public virtual void SwitchConstraint(bool value)
        {
        }
        
        public void SwitchShadow(bool value) => _meshRenderer.shadowCastingMode = value ? ShadowCastingMode.On : ShadowCastingMode.Off;

    }
}