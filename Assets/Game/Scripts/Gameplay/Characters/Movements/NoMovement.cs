﻿using System;
using UnityEngine;

namespace Gameplay.Characters.Movements
{
	public class NoMovement : MovementBehaviour
	{
		public override void Move(Vector3 input, Action OnEndPath = null)
		{
		}

		public override void Warp(Vector3 input)
		{
		}

		public override bool IsStopped => true;
	}
}