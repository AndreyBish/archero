﻿using UnityEngine;

namespace Gameplay.Characters.Enemies
{
    public class EnemyJumper: EnemyBase
    {
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Move(Vector3.forward);
            }
            
            if (Input.GetKeyDown(KeyCode.R))
            {
                _movement.Disable();
            }
            
        }
    }
}