﻿using System.Collections;
using Common;
using UI.Windows;
using UnityEngine;

namespace UI.Buttons
{
	public class HideAllUIButton : MyButton
	{
		private SceneUI        _sceneUi;
		private Coroutine      _checkCornerClickLoop;

		protected override void Awake()
		{
			base.Awake();

			_sceneUi = FindObjectOfType<SceneUI>();
		}

		protected override void ClickButton()
		{
			base.ClickButton();

			if (_sceneUi != null)
			{
				_sceneUi.SetActiveAllWindow(false);

				if (_checkCornerClickLoop != null) StopCoroutine(_checkCornerClickLoop);

				_checkCornerClickLoop = _sceneUi.StartCoroutine(CheckCornerClickLoop());
			}
		}

		private IEnumerator CheckCornerClickLoop()
		{
			var clickBoxSize = Mathf.Min(Screen.height, Screen.width) / 5f;

			while (true)
			{
				if (Input.GetMouseButtonDown(0))
				{
					var mousePosition = Input.mousePosition;

					if (mousePosition.x > Screen.width - clickBoxSize && mousePosition.y > Screen.height - clickBoxSize)
					{
						_sceneUi.SetActiveAllWindow(true);
						yield break;
					}
				}

				yield return null;
			}
		}
	}
}