using System.Collections;
using DG.Tweening;
using Extensions;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Buttons
{
	public class SettingsExpandButton : MyButton, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
	{
		[SerializeField, GroupComponent] private RectTransform      _myRect;
		[SerializeField, GroupComponent] private RectTransform      _expandRect;
		[SerializeField, GroupComponent] private CanvasGroup        _expandCanvas;
		[SerializeField, GroupComponent] private HideSettingsButton _settingsButton;

		[SerializeField, GroupSetting] private float _openDuration;

		private bool  _isActivated;
		private float _holdTime;
		private bool  _isPressed;
		private Tween _tweenCanvas;
		private Tween _tweenBottom;

		protected override void Awake()
		{
			base.Awake();

			_settingsButton.OnClick += CloseSettings;
			StartCoroutine(CloseSettingsCor());
		}

		private IEnumerator CloseSettingsCor()
		{
			yield return new WaitWhile(() => _expandRect.rect.height == 0);

			CloseSettings();
		}

		private void CloseSettings()
		{
			_isActivated = false;
			SwitchExpandMenu(false, true);
		}

		protected override void ClickButton()
		{
			base.ClickButton();

			_isActivated = !_isActivated;

			SwitchExpandMenu(_isActivated);
		}

		private void SwitchExpandMenu(bool active, bool force = false)
		{
			_tweenCanvas?.Kill();
			_tweenCanvas = _expandCanvas.transform.DOLocalMoveY(active ? 0 : _expandRect.rect.height,
																												  force ? 0 : _openDuration).SetEase(Ease.OutExpo).
																	 OnComplete(() => SetEnableClick(active));

			_tweenBottom?.Kill();
			_tweenBottom = DOTween.To(x => _myRect.offsetMin = Vector2.down * x,
																-_myRect.offsetMin.y,
																active ? _expandRect.rect.height : 0,
																force ? 0 : _openDuration).SetEase(Ease.OutExpo);
		}

		private void SetEnableClick(bool value)
		{
			_expandCanvas.interactable = value;
			_expandCanvas.blocksRaycasts = value;
		}

		private void Update()
		{
			if (_isPressed)
			{
				_holdTime += Time.deltaTime;
				if (_holdTime >= 5)
				{
#if FLAG_APPLOVIN
                MaxSdk.ShowMediationDebugger();
#endif
					Debug.Log("ShowMediationDebugger");
					//devPanelButton.IsEnable = true;
					_isPressed = false;
					_holdTime = 0;
				}
			}
		}

		public void OnPointerDown(PointerEventData eventData) => _isPressed = true;
		public void OnPointerUp(PointerEventData eventData) => _isPressed = false;
		public void OnPointerExit(PointerEventData eventData) => _isPressed = false;
	}
}