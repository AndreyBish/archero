﻿using Common;
using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Buttons
{
	public class VibroButton : MyButton
	{
		[SerializeField, GroupComponent] private Image  _targetImage;
		[SerializeField, GroupAssets]    private Sprite _onImage;
		[SerializeField, GroupAssets]    private Sprite _offImage;

		protected override void ClickButton()
		{
			base.ClickButton();

			Settings.VibrationEnabled = !Settings.VibrationEnabled;

			if (Settings.VibrationEnabled) MyVibration.Haptic(MyHapticTypes.LightImpact);

			ChangeImage();
		}

		private void ChangeImage()
		{
			_targetImage.sprite = Settings.VibrationEnabled ? _onImage : _offImage;
		}

		private void Start()
		{
			ChangeImage();
		}
	}
}