﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Buttons
{
    public class MyButtonPress: MyButton,  IPointerDownHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            PressButton();
        }
    }
}