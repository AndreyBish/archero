﻿using Common;
using TMPro;
using UI.Buttons;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Windows
{
	public class VictoryWindow : CanvasBasedWindow
	{
		[Space]
		[SerializeField] private MyButton _continueButton;

		[SerializeField] private TextMeshProUGUI _completedLevelNumberText;
		[SerializeField] private Image _locationImage;
		[SerializeField] private TextMeshProUGUI _locationName;
		

		public MyButton ContinueButton => _continueButton;

		public sealed override void Enable(bool value, float delay = -1, float duration = -1)
		{
			base.Enable(value, delay, duration);

			if (value)
			{
				_continueButton.Show();
			}
			else
			{
				_continueButton.Hide();
			}

			if (enabled)
			{
				UpdateLevelText();
			}
		}

		private void OnEnable()
		{
		}

		private void OnDisable()
		{
		}

		private void RewardedLoaded(bool flag)
		{
			// КнопкаРевардед.interactable = flag;
		}

		private void UpdateLevelText()
		{
			_completedLevelNumberText.text = $"LEVEL {Statistics.PlayerLevel - 1} COMPLETED";
		}

		protected override void Awake()
		{
			base.Awake();

			ShowDelayWps = new WaitForSeconds(_showDelay);
		}

		public void SetLocation(Sprite sprite, string locationName)
		{
			_locationImage.sprite = sprite;
			_locationName.text = locationName;
		}
	}
}