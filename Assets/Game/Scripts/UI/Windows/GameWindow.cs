﻿using System.Collections;
using Common;
using DG.Tweening;
using Gameplay;
using TMPro;
using UI.Buttons;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Windows
{
	public class GameWindow : CanvasBasedWindow
	{

		[SerializeField] private CanvasGroup _gameWindowCanvasGroup;
		
		[Header("Level Text")]
		[SerializeField] private TextMeshProUGUI _currentLevelText;

		[SerializeField] private float _hideLevelTextInterval;

		[Header("Starter")]
		//[SerializeField] private ActionToPlay _actionToPlay;
		[SerializeField] private bool  _blockedControl = false;
		[SerializeField] private float _hideViewTime   = 0.5f;

		//public ActionToPlay ActionToPlay => _actionToPlay;

		private WaitForSeconds _hideLevelTextIntervalWfs;
		private Coroutine      _hideLevelTextCoroutine;

		public sealed override void Enable(bool value, float delay = -1, float duration = -1)
		{
			base.Enable(value, delay, duration);

			if (!enabled) return;

			UpdateLevelText();
			ShowLevelText();
		}

		protected override void Awake()
		{
			base.Awake();

			//_actionToPlay.Init(_blockedControl, _hideViewTime);

			_hideLevelTextIntervalWfs = new WaitForSeconds(_hideLevelTextInterval);
		}

		public void HideLevelTextAfterDelay()
		{
			_hideLevelTextCoroutine = StartCoroutine(HideLevelTextAfterDelayRoutine());
		}

		private void ShowLevelText()
		{
			if (_hideLevelTextCoroutine != null)
			{
				StopCoroutine(_hideLevelTextCoroutine);
				_currentLevelText.DOKill();
			}

			_currentLevelText.color = Color.white;
		}

		private IEnumerator HideLevelTextAfterDelayRoutine()
		{
			yield return _hideLevelTextIntervalWfs;

			_currentLevelText.DOColor(Color.clear, 0.25f).OnComplete(() => _hideLevelTextCoroutine = null);
		}

		private void UpdateLevelText()
		{
			_currentLevelText.text = $"LEVEL {Statistics.PlayerLevel}";
		}
	}
}