﻿using TMPro;
using UI.Buttons;
using UnityEngine;

namespace UI.Windows
{
	public class LosingWindow : CanvasBasedWindow
	{
		[Space]
		[SerializeField] private MyButton _restartButton;

		[SerializeField] private TextMeshProUGUI _fadedText;
		[SerializeField] private TextMeshProUGUI _cost;

		public MyButton RestartButton => _restartButton;

		public sealed override void Enable(bool value, float delay = -1, float duration = -1)
		{
			base.Enable(value, delay, duration);

			if (value)
			{
				_restartButton.Show();
			}
			else
			{
				_restartButton.Hide();
			}
		}

		public void SetCost(int cost) => _cost.text = "+ " + cost;
	}
}