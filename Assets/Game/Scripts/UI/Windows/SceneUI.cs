﻿using System;
using Gameplay;
using Infrastructure;
using UnityEngine;

namespace UI.Windows
{
	public class SceneUI : MonoBehaviour
	{
		public event Action OnTapToPlayClicked;
		public event Action OnRestartInGameClicked;
		public event Action OnRestartOnLoseClicked;
		public event Action OnContinueClicked;

		[SerializeField] private GameWindow    _gameWindow;
		[SerializeField] private VictoryWindow _victoryWindow;
		[SerializeField] private LosingWindow  _losingWindow;

		private LevelManagerController _levelManagerController;

		public GameWindow GameWindow => _gameWindow;
		public LosingWindow LosingWindow => _losingWindow;
		public VictoryWindow VictoryWindow => _victoryWindow;

		public void Init(LevelManagerController levelManagerController)
		{
			//ActionToPlay.OnComplete += ProcessTapToPlayClicked;
			_losingWindow.RestartButton.OnClick += ProcessRestartOnLoseClicked;
			_victoryWindow.ContinueButton.OnClick += ProcessContinueClicked;

			_levelManagerController = levelManagerController;
			_levelManagerController.Current.OnLevelLoaded += ProcessLevelLoaded;
			_levelManagerController.Current.OnLevelCompleted += ProcessLevelCompleted;
			_levelManagerController.Current.OnLevelNotPassed += ProcessLevelLose;

			HideAll();
		}

		private void ProcessTapToPlayClicked()
		{
			//_gameWindow.ActionToPlay.HideView();
			_gameWindow.HideLevelTextAfterDelay();

			OnTapToPlayClicked?.Invoke();
		}

		private void ProcessRestartInGameClicked() => OnRestartInGameClicked?.Invoke();
		private void ProcessRestartOnLoseClicked() => OnRestartOnLoseClicked?.Invoke();
		private void ProcessContinueClicked() => OnContinueClicked?.Invoke();

		private void HideAll()
		{
			_gameWindow.Enable(false);
			_victoryWindow.Enable(false);
			_losingWindow.Enable(false);
		}

		public void SetActiveAllWindow(bool value)
		{
			_gameWindow.gameObject.SetActive(value);
			_victoryWindow.gameObject.SetActive(value);
			_losingWindow.gameObject.SetActive(value);
		}

		private void ProcessLevelLoaded()
		{
			_losingWindow.Enable(false);
			_victoryWindow.Enable(false);
			_gameWindow.Enable(true);
			//_gameWindow.ActionToPlay.ShowView();

		}

		private void ProcessLevelCompleted()
		{

			_gameWindow.Enable(false);
			_victoryWindow.Enable(true);
		}

		private void ProcessLevelLose()
		{

			_gameWindow.Enable(false);
			_losingWindow.Enable(true);
		}

	}
}