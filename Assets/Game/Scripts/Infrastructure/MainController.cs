﻿using System.Collections;
using Common;
using Common.ObjectPool;
using DG.Tweening;
using UI.Windows;
using UnityEngine;
using UnityEngine.UI;

namespace Infrastructure
{
	public class MainController : MonoBehaviour
	{
		[SerializeField] private SceneUI                _sceneUI;
		[SerializeField] private Image                _nextLevelImage;
		[SerializeField] private LevelManagerController _levelManagerController;

		private void Awake()
		{
#if UNITY_WEBGL
			Application.targetFrameRate = 0;
#else
			Application.targetFrameRate = 60;
#endif
		}

		private void Start()
		{
			_levelManagerController.Initialize();
			_levelManagerController.Current.OnLevelLoaded += LevelManager_OnLevelLoaded;
			_levelManagerController.Current.OnLevelCompleted += LevelManager_OnLevelCompleted;
			_levelManagerController.Current.OnLevelNotPassed += LevelManager_OnLevelNotPassed;

			_sceneUI.Init(_levelManagerController);
			//_sceneUI.OnTapToPlayClicked += StartLevel;
			_sceneUI.OnRestartInGameClicked += RestartGameInGame;
			_sceneUI.OnContinueClicked += NextLevel;
			_sceneUI.OnRestartOnLoseClicked += RestartGameFailedScreen;

			LoadLevel();
		}

		private void LoadLevel()
		{
			_levelManagerController.Current.LoadLevel();
		}

		private void NextLevel()
		{
			Pool.ReleaseAll();
			_levelManagerController.Current.LoadLevel();
		}

		private IEnumerator NextLevelDelay()
		{
			_nextLevelImage.DOFade(1.0f, 0.25f);
			yield return new WaitForSeconds(1.0f);
			_nextLevelImage.DOFade(0.0f, 0.25f);
			
		}

		private void RestartGame(bool gameScreen)
		{
			Pool.ReleaseAll();
		}

		private void RestartGameFailedScreen()
		{
			RestartGame(false);
		}

		private void RestartGameInGame()
		{
			if (!_levelManagerController.Current.CurrentLevel.LevelStarted) return;

			RestartGame(true);
		}

		private void InterstitialClosed()
		{
			_levelManagerController.Current.LoadLevel();
		}

		private void LevelManager_OnLevelLoaded()
		{
			
		}

		private void StartLevel()
		{
			_levelManagerController.Current.CurrentLevel.StartGameProcess();
		}

		private void LevelManager_OnLevelCompleted()
		{
		
		}

		private void LevelManager_OnLevelNotPassed()
		{
			
		}
	}
}