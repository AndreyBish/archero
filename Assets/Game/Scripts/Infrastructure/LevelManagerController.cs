using Sirenix.OdinInspector;
using UnityEngine;

namespace Infrastructure
{
	public enum LevelManagerBaseType
	{
		Prefabs,
		Scenes
	}

	[ExecuteInEditMode]
	public class LevelManagerController : MonoBehaviour
	{
		[Header("Settings"), EnumToggleButtons]
		[SerializeField] private LevelManagerBaseType _baseType;

		private PrefabBasedLevelManager _prefabBasedLevelManager;

		private LevelManagerBase _current;

		public PrefabBasedLevelManager PrefabBasedLevelManager => _prefabBasedLevelManager;
		public LevelManagerBase Current => _current;

		public static LevelManagerBaseType BaseType;

		public void Initialize()
		{
			if (_prefabBasedLevelManager == null)
			{
				_prefabBasedLevelManager = GetComponent<PrefabBasedLevelManager>();
			}
		}

		private void CheckComponents()
		{
			var prefabBasedLevelManager = GetComponent<PrefabBasedLevelManager>();

			if (prefabBasedLevelManager == null)
			{
				prefabBasedLevelManager = gameObject.AddComponent<PrefabBasedLevelManager>();

				_current = prefabBasedLevelManager;
			}
			else
				_current = prefabBasedLevelManager;

			_prefabBasedLevelManager = prefabBasedLevelManager;


		}

		private void Awake()
		{
			BaseType = _baseType;

			CheckComponents();
		}

		private void OnValidate()
		{
			BaseType = _baseType;

			CheckComponents();
		}
	}
}