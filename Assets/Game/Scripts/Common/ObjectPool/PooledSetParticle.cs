﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using static Common.Enums;

namespace Common.ObjectPool
{
    public class PooledSetParticle: PooledParticle
    {
        [SerializeField] private List<PooledCube> _particleSystems;

        private ParticleSystem _currentParticleSystem;

        private Vector3 _startScale = new Vector3(0.8f, 0.8f, 0.8f);

        public override void SetParticleType(ParticleType particleType)
        {
            if(_currentParticleSystem!= null) _currentParticleSystem.gameObject.SetActive(false);
            _currentParticleSystem = _particleSystems.FirstOrDefault(x => x.ParticleType == particleType)?.ParticleSystem;
            if (_currentParticleSystem != null)
            {
                _currentParticleSystem.gameObject.SetActive(true);
            }
            base.SetParticleType(particleType);
        }

        public override void Restart()
        {
            if (_isResetScale) transform.localScale = _startScale;
            base.Restart();
        }

        public override void Retain(int id, string containerName)
        {
            
            base.Retain(id, containerName);
        }

        public override void Release(bool disableObject = true)
        {
            if (_isResetScale) transform.DOScale(0.0f, 0.25f).OnComplete(() => base.Release(disableObject));
            else base.Release(disableObject);
        }
    }

    [Serializable]
    public class PooledCube
    {
        [SerializeField] private ParticleType _particleType;
        [SerializeField] private ParticleSystem _particleSystem;

        public ParticleType ParticleType => _particleType;
        public ParticleSystem ParticleSystem => _particleSystem;
    }
}