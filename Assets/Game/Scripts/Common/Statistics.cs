﻿using UnityEngine;
using static Common.Constants.PlayerPrefsKeyNames;

namespace Common
{
	public static class Statistics
	{
		public static int PlayerLevel
		{
			get { return PlayerPrefs.GetInt(PLAYER_LEVEL, 1); }
			set
			{
				PlayerPrefs.SetInt(PLAYER_LEVEL, value);
				PlayerPrefs.Save();
			}
		}
		
		public static int DaysLived
		{
			get { return PlayerPrefs.GetInt(DAYS_LIVED, 0); }
			set
			{
				PlayerPrefs.SetInt(DAYS_LIVED, value);
				PlayerPrefs.Save();
			}
		}

		public static int CurrentLevelIndex
		{
			get { return PlayerPrefs.GetInt(CURRENT_LEVEL_NUMBER, 0); }
			set
			{
				PlayerPrefs.SetInt(CURRENT_LEVEL_NUMBER, value);
				PlayerPrefs.Save();
			}
		}
		
		public static int CurrentTowerLevel
		{
			get { return PlayerPrefs.GetInt(PLAYER_TOWER_LEVEL, 0); }
			set
			{
				PlayerPrefs.SetInt(PLAYER_TOWER_LEVEL, value);
				PlayerPrefs.Save();
			}
		}

		public static bool AllLevelsCompleted
		{
			get { return PlayerPrefs.GetInt(ALL_LEVELS_COMPLETED, 0) == 1; }
			set
			{
				PlayerPrefs.SetInt(ALL_LEVELS_COMPLETED, value ? 1 : 0);
				PlayerPrefs.Save();
			}
		}

		public static bool TutorialCompleted
		{
			get { return PlayerPrefs.GetInt(TUTORIAL_COMPLETED, 0) == 1; }
			set
			{
				PlayerPrefs.SetInt(TUTORIAL_COMPLETED, value ? 1 : 0);
				PlayerPrefs.Save();
			}
		}

		public static int CurrentTutorialGroup
		{
			get { return PlayerPrefs.GetInt(CURRENT_TUTORIAL_GROUP, 0); }
			set
			{
				PlayerPrefs.SetInt(CURRENT_TUTORIAL_GROUP, value);
				PlayerPrefs.Save();
			}
		}
	}
}