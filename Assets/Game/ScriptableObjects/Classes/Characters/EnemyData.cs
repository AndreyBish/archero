﻿using UnityEngine;

namespace ScriptableObjects.Classes.Characters
{
    [CreateAssetMenu(fileName = "EnemyData", menuName = "Data/Characters/EnemyData")]
    public class EnemyData: CharacterData
    {
        
    }
}