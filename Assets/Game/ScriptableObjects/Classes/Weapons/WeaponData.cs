﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using static Common.Enums;

namespace Game.ScriptableObjects.Classes.WeaponsData
{
    [CreateAssetMenu(fileName = "WeaponData", menuName = "Data/WeaponData")]
    public class WeaponData : ScriptableObject
    {
        public static event Action<WeaponData> OnChangeOffset;

        [SerializeField] private WeaponType _type;
        [SerializeField] private TeamType   _team;
        [SerializeField] private BulletType _bulletType;
        [SerializeField] private Mesh       _mesh;
        // [SerializeField] private int      _damage;
        //
        // [SerializeField, ShowIf(nameof(IsGun))] private float _bulletSpeed;
        // [SerializeField, ShowIf(nameof(IsGun))] private float _shotDelay;

        [SerializeField, OnValueChanged(nameof(ChangeOffset))] private Vector3 _positionOffset;
        [SerializeField, OnValueChanged(nameof(ChangeOffset))] private Vector3 _rotationOffset;

        [SerializeField, OnValueChanged(nameof(ChangeOffset)), ShowIf(nameof(IsGun))] private Vector3 _gunEndPosition;
        private bool IsGun => Type != WeaponType.None;
        public WeaponType Type => _type;
        public TeamType Team => _team;
        public BulletType BulletType => _bulletType;
        public Mesh Mesh => _mesh;
        // public float BulletSpeed => _bulletSpeed;
        // public int Damage => _damage;
        // public float ShotDelay => _shotDelay;
        public Vector3 PositionOffset => _positionOffset;
        public Vector3 RotationOffset => _rotationOffset;
        public Vector3 GunEndPosition => _gunEndPosition;
        private void ChangeOffset() => OnChangeOffset?.Invoke(this);
    }
}