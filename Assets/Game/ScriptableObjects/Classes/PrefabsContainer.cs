using UnityEngine;
using System.Collections.Generic;
using Gameplay;
using Sirenix.OdinInspector;
using Sirenix.Utilities;

[CreateAssetMenu(menuName = "Configs/Prefabs Container")]
public class PrefabsContainer : GlobalConfig<PrefabsContainer>
{
	[SerializeField]
	private bool isDebug;
	[SerializeField, ShowIf("isDebug"), ValueDropdown("levels")]
	private Level debugLevel;
	[SerializeField]
	private List<Level> levels;

	public List<Level> Levels => levels;

	public bool IsDebug
	{
		get => isDebug;
		set => isDebug = value;
	}

	public Level DebugLevel => debugLevel;
}