﻿using System;
using System.Collections.Generic;
using Common.ObjectPool;
using Gameplay.Characters.Enemies;
using Gameplay.Weapons.Bullets;
using ScriptableObjects.Classes.Characters;
using Sirenix.OdinInspector;
using UnityEngine;
using static Common.Enums;

namespace ScriptableObjects.Classes.Prefabs
{
	[CreateAssetMenu(fileName = "PrefabData", menuName = "Data/PrefabData", order = 0)]
	public class PrefabData : SerializedScriptableObject
	{
		[SerializeField] private Dictionary<ParticleType, PooledParticle> _particlePrefabs;
		[SerializeField] private Dictionary<BulletType, BulletBase> _bulletPrefabs;
		public Dictionary<ParticleType, PooledParticle> ParticlePrefabs => _particlePrefabs;
		public Dictionary<BulletType, BulletBase> BulletPrefabs => _bulletPrefabs;
		[SerializeField, TableList] private List<EnemyElement> _enemyPrefabs;

		[Serializable]
		public class EnemyElement
		{
			[SerializeField] private EnemyType _type;
			[SerializeField] private EnemyBase _prefab;
			[SerializeField] private EnemyData _data;
			public EnemyType Type => _type;
			public EnemyBase Prefab => _prefab;
			public EnemyData Data => _data;
		}
	}
}